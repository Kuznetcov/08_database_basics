﻿INSERT INTO "People" (name) VALUES
 ('Lunohod1'),
 ('Lunohod2'),
 ('Lunohod3'),
 ('Lunohod4'),
 ('Lunohod5'),
 ('Lunohod6'),
 ('Lunohod7'),
 ('Lunohod8'),
 ('Lunohod9'),
 ('Lunohod10'),
 ('Lunohod11'),
 ('Lunohod12'),
 ('Lunohod13'),
 ('Lunohod14'),
 ('Lunohod15'),
 ('Lunohod16'),
 ('Lunohod17'),
 ('Lunohod18'),
 ('Lunohod19'),
 ('Lunohod20'),
 ('Lunohod21'),
 ('Lunohod22'),
 ('Lunohod23'),
 ('Lunohod24'),
 ('Lunohod25'),
 ('Lunohod26'),
 ('Lunohod27'),
 ('Lunohod28'),
 ('Lunohod29'),
 ('Lunohod30'),
 ('Lunohod31'),
 ('Lunohod32'),
 ('Lunohod33'),
 ('Lunohod34');

 
INSERT INTO "Phones" VALUES
 ('89120000021',1),
 ('89120000020',2),
 ('89120000019',3),
 ('89120000018',4),
 ('89120000017',5),
 ('89120000016',6),
 ('89120000015',7),
 ('89120000014',8),
 ('89120000013',9),
 ('89120000012',10),
 ('89120000011',11),
 ('89120000010',12),
 ('89120000009',13),
 ('89120000008',14),
 ('89120000007',15),
 ('89120000006',16),
 ('89120000005',17),
 ('89120000004',18),
 ('89120000003',19),
 ('89120000002',20),
 ('89120000001',21);



 INSERT INTO "Client" VALUES
 (1),
 (2),
 (3),
 (4),
 (5),
 (6),
 (7),
 (8),
 (9),
 (10);

 INSERT INTO "Employees" VALUES
 (11,DEFAULT),
 (12,DEFAULT),
 (13,DEFAULT),
 (14,DEFAULT),
 (15,DEFAULT),
 (16,DEFAULT),
 (17,DEFAULT),
 (18,DEFAULT),
 (19,DEFAULT),
 (20,DEFAULT);

 INSERT INTO "Cars" (owner) VALUES
 (1),
 (2),
 (3),
 (4),
 (5),
 (6),
 (7),
 (8),
 (9),
 (10);

 INSERT INTO "ParkingPlace" (available) VALUES
 (DEFAULT),
 (DEFAULT),
 (DEFAULT),
 (DEFAULT),
 (DEFAULT),
 (DEFAULT),
 (DEFAULT),
 (DEFAULT),
 (DEFAULT),
 (DEFAULT);
 
 INSERT INTO "EventType" (name,bloking,unbloking) VALUES
 ('CAR ARRIVED',TRUE,FALSE),
 ('CAR GONE',FALSE,TRUE),
 ('PLACEHOLDER',FALSE,FALSE);
 
 
 
 