﻿CREATE VIEW EmployeesView AS
SELECT 
  "Employees".id, 
  "People".name, 
  "Employees".salary, 
  "Phones".phone
FROM 
  public."Employees", 
  public."People", 
  public."Phones"
WHERE 
  "People".id = "Employees".id AND
  "Phones".owner = "People".id;
