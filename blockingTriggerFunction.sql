﻿CREATE OR REPLACE FUNCTION unblockPlace() RETURNS TRIGGER AS $$

BEGIN

	
IF (
(SELECT bloking FROM "EventType" WHERE name = NEW.event)
) 
THEN
    UPDATE "ParkingPlace"
	SET available=FALSE WHERE id=NEW.place;
	RETURN NEW;
END IF;

IF (
(SELECT unbloking FROM "EventType" WHERE name = NEW.event)
) 
THEN
    UPDATE "ParkingPlace"
	SET available=TRUE WHERE id=NEW.place;
	RETURN NEW;
END IF;


END
$$ LANGUAGE plpgsql;