﻿
CREATE UNIQUE INDEX deal
  ON "Deals"
  USING btree
  (id);

  
CREATE INDEX phone
  ON "Phones"
  USING btree
  (phone COLLATE pg_catalog."default");
