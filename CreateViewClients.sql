﻿CREATE VIEW ClientsView AS
SELECT 
  "People".name AS "ФИО", 
  "Cars"."number" AS "Номер машины", 
  "Cars".model AS "Модель"
FROM 
  public."Client", 
  public."People", 
  public."Cars"
WHERE 
  "Client".id = "Cars".owner AND
  "People".id = "Client".id;
