﻿CREATE TABLE "Cars" (
	"number" SERIAL NOT NULL,
	"owner" integer NOT NULL,
	"model" varchar(20),
	CONSTRAINT Cars_pk PRIMARY KEY ("number")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Client" (
	"id" SERIAL NOT NULL,
	CONSTRAINT Client_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "ParkingPlace" (
	"id" SERIAL NOT NULL,
	"available" BOOLEAN NOT NULL DEFAULT 'TRUE',
	"owner" integer,
	CONSTRAINT ParkingPlace_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Employees" (
	"id" SERIAL NOT NULL,
	"salary" integer DEFAULT '10000',
	CONSTRAINT Employees_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Deals" (
	"id" SERIAL NOT NULL,
	"date" date NOT NULL,
	"client" integer NOT NULL,
	"employee" integer NOT NULL,
	"place" integer NOT NULL,
	"car" integer NOT NULL,
	CONSTRAINT Deals_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "PlaceEvents" (
	"id" SERIAL NOT NULL,
	"place" integer NOT NULL,
	"date" date NOT NULL,
	"event" varchar(20) NOT NULL,
	CONSTRAINT PlaceEvents_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Phones" (
	"phone" varchar(20) NOT NULL,
	"owner" integer NOT NULL
) WITH (
  OIDS=FALSE
);



CREATE TABLE "People" (
	"id" SERIAL NOT NULL,
	"name" varchar(50) NOT NULL,
	CONSTRAINT People_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);


CREATE TABLE "EventType" (
	"name" varchar(20) NOT NULL,
	"bloking" BOOLEAN NOT NULL DEFAULT 'FALSE',
	"unbloking" BOOLEAN NOT NULL DEFAULT 'FALSE',
	CONSTRAINT EventType_pk PRIMARY KEY ("name")
) WITH (
  OIDS=FALSE
);

ALTER TABLE "Cars" ADD CONSTRAINT "Cars_fk0" FOREIGN KEY ("owner") REFERENCES "Client"("id");

ALTER TABLE "Client" ADD CONSTRAINT "Client_fk0" FOREIGN KEY ("id") REFERENCES "People"("id");


ALTER TABLE "Employees" ADD CONSTRAINT "Employees_fk0" FOREIGN KEY ("id") REFERENCES "People"("id");

ALTER TABLE "Deals" ADD CONSTRAINT "Deals_fk0" FOREIGN KEY ("employee") REFERENCES "Employees"("id");
ALTER TABLE "Deals" ADD CONSTRAINT "Deals_fk1" FOREIGN KEY ("place") REFERENCES "ParkingPlace"("id");
ALTER TABLE "Deals" ADD CONSTRAINT "Deals_fk2" FOREIGN KEY ("car") REFERENCES "Cars"("number");

ALTER TABLE "PlaceEvents" ADD CONSTRAINT "PlaceEvents_fk0" FOREIGN KEY ("place") REFERENCES "ParkingPlace"("id");
ALTER TABLE "PlaceEvents" ADD CONSTRAINT "PlaceEvents_fk1" FOREIGN KEY ("event") REFERENCES "EventType"("name");

ALTER TABLE "Phones" ADD CONSTRAINT "Phones_fk0" FOREIGN KEY ("owner") REFERENCES "People"("id");
